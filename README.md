# fishy_fractal

In this project I'm exploring a way of colouring triangles.

I created `12_expected.png` using https://khalilfazal.gitlab.io/triangulart/

Requirements: numpy, drawSvg, Wand

![animation](animation.svg)
![final](final.svg)
