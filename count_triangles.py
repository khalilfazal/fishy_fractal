#!/bin/env python3

from collections import defaultdict
from hsv import HSV
import numpy as np
import xml.etree.ElementTree as ET

# open 44.svg
# create histogram = map<color, int>
#
# for each p = path:
#   histogram[p.color]++
#
# increment = 8
#
# for i in (0..44):
#   color = HSV(increment * i)
#   print(i, histogram[color])

if __name__ == "__main__":
    histogram = defaultdict(lambda: 0)
    svg = ET.parse('44.svg')
    ns = {'default': 'http://www.w3.org/2000/svg'}
    paths = svg.findall('default:path', ns)

    for p in paths:
        histogram[p.attrib['fill']] += 1

    increment = 8
    seq = []

    for i in range(0, 44):
        color = str(HSV(increment * i))
        seq.append(histogram[color])

    print(seq)
    print(np.diff(seq))