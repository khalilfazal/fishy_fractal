#!/usr/bin/env python3

import colorsys
import itertools
import math
import numpy as np
import sys

import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = 'hide'

# https://stackoverflow.com/a/40137700
os.environ['PYTHONHASHSEED'] = '0'

import pygame

class HSV(pygame.Color):
    def __init__(self, hue):
        self.hue = hue
        r, g, b = [int(255 * c) for c in colorsys.hsv_to_rgb(self.hue / 360, 1, 1)]
        super().__init__(r, g, b)

    def __add__(self, other):
        return HSV(self.hue + other)

class Triangle:
    def __init__(self, top, bottom, fill, increment):
        self.top = top
        self.bottom = bottom
        self.fill = fill
        self.increment = increment

        # https://qr.ae/prnqFc
        i1x = (bottom[0] + top[0])/2 + math.sqrt(3)/2*(bottom[1] - top[1])
        i1y = (bottom[1] + top[1])/2 - math.sqrt(3)/2*(bottom[0] - top[0])

        # https://math.stackexchange.com/a/1324213
        # choose the point that forms an angle in the counter-clockwise direction

        # i1: first intersection of circles centered at self.top and self.bottom
        # i2: second intersection of circles centered at self.top and self.bottom

        a = np.array([
            [self.top[0], self.top[1], 1],
            [self.bottom[0], self.bottom[1], 1],
            [i1x, i1y, 1]
        ])
        d = np.linalg.det(a)

        if d < 0:
            self.forward = (int(round(i1x)), int(round(i1y)))
        else:
            i2x = (bottom[0] + top[0])/2 - math.sqrt(3)/2*(bottom[1] - top[1])
            i2y = (bottom[1] + top[1])/2 + math.sqrt(3)/2*(bottom[0] - top[0])

            self.forward = (int(round(i2x)), int(round(i2y)))

    def __str__(self):
        return f"{self.fill},{self.top[0]},{self.top[0]},{self.bottom[0]},{self.bottom[1]},{self.forward[0]},{self.forward[1]}"

    def __repr__(self):
        return self.__str__()

    def prehash(self):
        l = list(self.top + self.bottom + self.forward)
        l.sort()
        return l

    def __hash__(self):
        return hash(str(self.prehash()))

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def draw(self, surface, offset):
        top = (self.top[0] - offset[0], self.top[1] - offset[1])
        bottom = (self.bottom[0] - offset[0], self.bottom[1] - offset[1])
        forward = (self.forward[0] - offset[0], self.forward[1] - offset[1])

        pygame.draw.polygon(surface=surface, color=self.fill, points=[top, bottom, forward])

    def min(self, dim):
        return min(self.top[dim], self.bottom[dim], self.forward[dim])

    def max(self, dim):
        return max(self.top[dim], self.bottom[dim], self.forward[dim])

    def left(self):
        return(Triangle(self.top, self.forward, self.fill + self.increment, self.increment))

    def right(self):
        return(Triangle(self.forward, self.bottom, self.fill + self.increment, self.increment))

class Canvas:
    def __init__(self, layers, keep, save=True):
        self.layers = layers
        self.keep = keep
        self.save = save

    def draw(self, triangles):
        min_x = min([triangle.min(0) for triangle in triangles])
        max_x = max([triangle.max(0) for triangle in triangles])
        width = max_x - min_x

        min_y = min([triangle.min(1) for triangle in triangles])
        max_y = max([triangle.max(1) for triangle in triangles])
        height = max_y - min_y

        screen = pygame.display.set_mode((width, height))

        for triangle in triangles:
            triangle.draw(screen, offset=(min_x, min_y))

        pygame.display.update()

        if self.save:
            pygame.image.save(screen, f"{self.layers}.png")

        try:
            while self.keep:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
        except KeyboardInterrupt:
            pygame.quit()

# 0 <= layers <= 45
def create_layers(triangle_side_length, starting_color, increment, save, keep, layers):
    canvas = Canvas(layers, keep, save)
    a = Triangle((0, 0), (0, triangle_side_length), starting_color, increment)

    # the same data is stored using two different structures
    # a list of layers of triangles
    triangles = [[a]]

    # and a flat list of triangles
    flat = [a]

    for i in range(0, layers):
        # generate a new layer of triangles using the previous layer only
        prev = triangles[-1]
        new_layer = []

        for triangle in prev:
            l = triangle.left()
            if l not in flat and l not in new_layer:
                new_layer.append(l)

            r = triangle.right()
            if r not in flat and r not in new_layer:
                new_layer.append(r)

        triangles.append(new_layer)
        flat += new_layer

    flat.reverse()
    canvas.draw(flat)

if __name__ == "__main__":
    layers = int(sys.argv[1])

    create_layers(
        triangle_side_length=20,
        starting_color=HSV(0),
        increment=8,
        save=True,
        keep=True,
        layers=layers
    )
